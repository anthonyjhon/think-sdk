<?php

namespace Henan\ThinkSdk\model;


use Henan\ThinkSdk\traits\ModelTrait;
use think\Model;
use think\model\concern\SoftDelete;

/**
 * 软删除模型
 * @author henan
 */
class SoftModel extends Model
{
    use ModelTrait;

    /**
     * 软删除
     */
    use SoftDelete;

    /**
     * 自动时间戳类型
     * @var string
     */
    protected $autoWriteTimestamp = true;

    /**
     * 添加时间字段
     * @var string
     */
    protected $createTime = 'create_time';

    /**
     * 更新时间字段
     * @var string
     */
    protected $updateTime = 'update_time';

    /**
     * 删除时间字段
     * @var string
     */
    protected string $deleteTime = 'delete_time';
}