<?php

namespace Henan\ThinkSdk\model;


use Henan\ThinkSdk\traits\ModelTrait;
use think\Model;

/**
 * 自动时间戳模型
 * @author henan
 */
class TimeModel extends Model
{
    use ModelTrait;

    /**
     * 自动时间戳类型
     * @var string
     */
    protected $autoWriteTimestamp = true;

    /**
     * 添加时间字段
     * @var string
     */
    protected $createTime = 'create_time';

    /**
     * 更新时间字段
     * @var string
     */
    protected $updateTime = 'update_time';
}