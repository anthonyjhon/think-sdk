<?php
// +----------------------------------------------------------------------
// | THINK-SDK 配置文件
// +----------------------------------------------------------------------

return [
    // 邮箱服务配置
    'EmailService' => [
        'username' => '',
        'password' => ''
    ],
    // 阿里云储存服务配置
    'OssService' => [
        'accessKeyId' => '',
        'accessKeySecret' => '',
        'endpoint' => '',
        'bucket' => ''
    ],
    // 钉钉机器人服务配置
    'DingBotService' => [
        // 默认机器人
        'default' => [
            'webhook' => '',
            'secret' => ''
        ]
    ],
    // 系统自定义日志配置
    'LogDefine' => [
        // 钉钉机器人
        'dingBot' => 'default',
        // 是否开启钉钉机器人通知
        'dingBotEnable' => false,
    ],
    // 回收站模型特征配置
    'RcyModelTrait' => [
        // 数据库名（注：见database配置文件）
        'connect' => 'mysql',
        // 记录数据表
        'table' => 'recycle',
    ]
];