<?php

namespace Henan\ThinkSdk\controller;

use Henan\ThinkSdk\traits\ResponseTrait;

class Api
{
    use ResponseTrait;

    /**
     * 欢迎接口
     * @return void
     */
    public function welcome(): void
    {
        $this->success('欢迎使用 Think-SDK');
    }
}