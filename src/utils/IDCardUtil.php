<?php

namespace Henan\ThinkSdk\utils;

use Exception;

/**
 * 身份证工具
 */
class IDCardUtil
{
    /**
     * 身份证号码
     * @var string
     */
    public string $number = '';

    /**
     * 构造方法
     * @param string $number 身份证号码
     * @throws Exception
     */
    public function __construct(string $number)
    {
        if (!self::verify($number)) throw new Exception('无效的身份证');
        $this->number = $number;
    }

    /**
     * 获取性别
     * @return string
     */
    public function sex(): string
    {
        $sexInt = (int)substr($this->number, 16, 1);
        return $sexInt % 2 === 0 ? '女' : '男';
    }

    /**
     * 获取年龄
     * @return int
     */
    public function age(): int
    {
        $date = strtotime(substr($this->number, 6, 8));
        $today = strtotime('today');
        $diff = floor(($today - $date) / 86400 / 365);
        return strtotime(substr($this->number, 6, 8) . ' +' . $diff . 'years') > $today ? ($diff + 1) : $diff;
    }

    /**
     * 获取生日
     * @return string
     */
    public function birthday(): string
    {
        $bir = substr($this->number, 6, 8);
        $year = (int)substr($bir, 0, 4);
        $month = (int)substr($bir, 4, 2);
        $day = (int)substr($bir, 6, 2);
        return $year . "-" . $month . "-" . $day;
    }

    /**
     * 获取生肖
     * @return string
     */
    public function zodiac(): string
    {
        $start = 1901;
        $end = (int)substr($this->number, 6, 4);
        $x = ($start - $end) % 12;
        $val = '';
        if ($x == 1 || $x == -11) $val = '鼠';
        if ($x == 0) $val = '牛';
        if ($x == 11 || $x == -1) $val = '虎';
        if ($x == 10 || $x == -2) $val = '兔';
        if ($x == 9 || $x == -3) $val = '龙';
        if ($x == 8 || $x == -4) $val = '蛇';
        if ($x == 7 || $x == -5) $val = '马';
        if ($x == 6 || $x == -6) $val = '羊';
        if ($x == 5 || $x == -7) $val = '猴';
        if ($x == 4 || $x == -8) $val = '鸡';
        if ($x == 3 || $x == -9) $val = '狗';
        if ($x == 2 || $x == -10) $val = '猪';
        return $val;
    }

    /**
     * 获取星座
     * @return string
     */
    public function starSign(): string
    {
        $b = substr($this->number, 10, 4);
        $m = (int)substr($b, 0, 2);
        $d = (int)substr($b, 2);
        $val = '';
        if (($m == 1 && $d <= 21) || ($m == 2 && $d <= 19)) {
            $val = "水瓶座";
        } else if (($m == 2 && $d > 20) || ($m == 3 && $d <= 20)) {
            $val = "双鱼座";
        } else if (($m == 3 && $d > 20) || ($m == 4 && $d <= 20)) {
            $val = "白羊座";
        } else if (($m == 4 && $d > 20) || ($m == 5 && $d <= 21)) {
            $val = "金牛座";
        } else if (($m == 5 && $d > 21) || ($m == 6 && $d <= 21)) {
            $val = "双子座";
        } else if (($m == 6 && $d > 21) || ($m == 7 && $d <= 22)) {
            $val = "巨蟹座";
        } else if (($m == 7 && $d > 22) || ($m == 8 && $d <= 23)) {
            $val = "狮子座";
        } else if (($m == 8 && $d > 23) || ($m == 9 && $d <= 23)) {
            $val = "处女座";
        } else if (($m == 9 && $d > 23) || ($m == 10 && $d <= 23)) {
            $val = "天秤座";
        } else if (($m == 10 && $d > 23) || ($m == 11 && $d <= 22)) {
            $val = "天蝎座";
        } else if (($m == 11 && $d > 22) || ($m == 12 && $d <= 21)) {
            $val = "射手座";
        } else if (($m == 12 && $d > 21) || ($m == 1 && $d <= 20)) {
            $val = "魔羯座";
        }
        return $val;
    }

    /**
     * 身份证号码校验
     * @param string $number 身份证号码
     * @return bool
     */
    public static function verify(string $number): bool
    {
        // 老身份证长度15位，新身份证长度18位
        $length = strlen($number);
        if ($length == 15) {
            if (!is_numeric($number)) return false; // 15位身份证没有字母
            $areaNum = substr($number, 0, 6); // 省市县（6位）
            $dateNum = substr($number, 6, 6); // 出生年月（6位）
        } else if ($length == 18) {
            if (!preg_match('/^\d{17}[0-9xX]$/', $number)) return false; // 基本格式校验
            $areaNum = substr($number, 0, 6); // 省市县（6位）
            $dateNum = substr($number, 6, 8); // 出生年月日（8位）
        } else {
            return false;
        }
        if (!self::isAreaCodeValid($areaNum)) return false; // 省市自治区合法性校验
        if (!self::isDateValid($dateNum)) return false; // 出生日期合法性校验
        if (!self::isVerifyCodeValid($number)) return false; // 18位身份证最后一位合法性校验
        return true;
    }

    /**
     * 省市自治区合法性校验
     * @param string $area 省、直辖市代码
     * @return bool
     */
    private static function isAreaCodeValid(string $area): bool
    {
        $provinceCode = substr($area, 0, 2);
        // 根据GB/T2260—999，省市代码11到65
        if (11 <= $provinceCode && $provinceCode <= 65) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 出生日期合法性校验
     * @param string $date 日期
     * @return bool
     */
    private static function isDateValid(string $date): bool
    {
        if (strlen($date) == 6) $date = '19' . $date; // 15位身份证号没有年份，这里拼上年份
        $year = intval(substr($date, 0, 4));
        $month = intval(substr($date, 4, 2));
        $day = intval(substr($date, 6, 2));
        if (!checkdate($month, $day, $year)) return false; // 日期基本格式校验
        // 日期格式正确，但是逻辑存在问题(如:年份大于当前年)
        $currYear = date('Y');
        if ($year > $currYear) return false;
        return true;
    }

    /**
     * 18位身份证最后一位合法性校验
     * @param string $number 待校验的身份证号
     * @return bool
     */
    private static function isVerifyCodeValid(string $number): bool
    {
        if (strlen($number) == 18) {
            $factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
            $tokens = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];
            $checkSum = 0;
            for ($i = 0; $i < 17; $i++) {
                $checkSum += intval($number[$i]) * $factor[$i];
            }
            $mod = $checkSum % 11;
            $token = $tokens[$mod];
            $lastChar = strtoupper($number[17]);
            if ($lastChar != $token) return false;
        }
        return true;
    }
}