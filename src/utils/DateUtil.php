<?php

namespace Henan\ThinkSdk\utils;

/**
 * 日期时间工具
 */
class DateUtil
{
    /**
     * 获取月份日期范围
     * @param string $month 月份
     * @return array
     */
    public static function getMonthRange(string $month): array
    {
        $start = date('Y-m-d', strtotime("$month-01"));
        $end = date('Y-m-d', strtotime("last day of $month"));
        return [$start, $end];
    }

    /**
     * 获取指定天数的日历
     * @param int $days 天数
     * @param int $offset 偏移量
     * @return array
     */
    public static function getCalendarByDays(int $days = 30, int $offset = 0): array
    {
        $list = [];
        $symbol = $days > 0 ? '+' : '-';
        $time = strtotime("$offset day", time());
        $num = 0;
        $sum = abs($days);
        $weeks = ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'];
        while ($num < $sum) {
            $unix = strtotime("$symbol $num day", $time);
            $week = date('w', $unix);
            $date = date('Y-m-d', $unix);
            $month = date('m', $unix);
            $day = date('d', $unix);
            $list[] = ['date' => $date, 'week' => $weeks[$week], 'month' => $month, 'day' => $day];
            $num++;
        }
        return $list;
    }
}