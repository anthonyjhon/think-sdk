<?php

namespace Henan\ThinkSdk\define;


use Henan\ThinkSdk\service\DingBotService;
use think\contract\LogHandlerInterface;
use think\facade\Config;

/**
 * 自定义日志类
 * @author henan
 */
class LogDefine implements LogHandlerInterface
{
    public function save(array $log): bool
    {
        $config = Config::get('sdk.LogDefine');
        if ($config['dingBotEnable'] && $config['dingBot']) {
            foreach ($log as $level => $content) {
                foreach ($content as $item) {
                    try {
                        (new DingBotService())->send('系统错误', $item, $config['dingBot']);
                    } catch (\Exception) {
                    }
                }
            }
        }
        return true;
    }
}