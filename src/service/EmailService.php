<?php

namespace Henan\ThinkSdk\service;


use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use think\facade\Config;

/**
 * 邮箱服务类
 * @author henan
 */
class EmailService
{
    /**
     * 构造函数
     * @param array $config
     */
    public function __construct(public array $config = [])
    {
        if (empty($this->config)) $this->config = Config::get('sdk.EmailService');
    }

    /**
     * 发送验证码
     * @param string $email 邮箱账号
     * @param string $name 发送人昵称
     * @param string $subject 邮件标题
     * @return bool
     * @throws Exception
     */
    public function sendVerificationCode(string $email, string $name = '', string $subject = ''): bool
    {
        try {
            $config = $this->config;
            $mail = new PHPMailer();           //实例化PHPMailer对象
            $mail->CharSet = 'UTF-8';           //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
            $mail->IsSMTP();                    // 设定使用SMTP服务
            $mail->SMTPDebug = 0;               // SMTP调试功能 0=关闭 1 = 错误和消息 2 = 消息
            $mail->SMTPAuth = true;             // 启用 SMTP 验证功能
            $mail->SMTPSecure = 'ssl';          // 使用安全协议
            $mail->Host = "smtp.qq.com";        // 企业邮局域名
            $mail->Port = 465;                  //设置ssl连接smtp服务器的远程服务器端口号 可选465或587
            $mail->FromName = $name;   // 发件人昵称
            $mail->Username = $config['username'];    // 发件人用户名(请填写完整的email地址)
            $mail->Password = $config['password'];    // 发件人密码（授权码）
            $mail->SetFrom($config['username'], $name);
            $replyEmail = '';                   //留空则为发件人EMAIL
            $replyName = '';                    //回复名称（留空则为发件人名称）
            $mail->AddReplyTo($replyEmail, $replyName);  //回复的地址
            $mail->Subject = $subject;   //邮件标题
            $code = rand(1000, 9999);
            cache("email_code_$email", $code, 600);
            $mail->MsgHTML(self::tplCode($code, $name));       //邮件内容
            $mail->AddAddress($email);  //收件人地址，("收件人email","收件人姓名")
            $res = $mail->Send();
        } catch (Exception $e) {
            throw new Exception(($e->getMessage()));
        }
        return $res;
    }

    /**
     * 校验验证码
     * @param string $email 邮箱账号
     * @param string $code 验证码
     * @return bool
     */
    public function checkVerificationCode(string $email, string $code): bool
    {
        return cache("email_code_$email") == $code;
    }

    /**
     * 验证码模板
     * @param string $code
     * @param string $name
     * @return string
     */
    public function tplCode(string $code, string $name = ''): string
    {
        return '<!DOCTYPE html><html lang="en"xmlns:th="http://www.thymeleaf.org"><head><meta charset="UTF-8"><title>邮箱验证码</title><style>table{width:700px;margin:0 auto}#top{width:700px;border-bottom:1px solid#ccc;margin:0 auto 30px}#top table{font:12px Tahoma,Arial,宋体;height:40px}#content{width:680px;padding:0 10px;margin:0 auto}#content_top{line-height:1.5;font-size:14px;margin-bottom:25px;color:#4d4d4d}#content_top strong{display:block;margin-bottom:15px}#content_top strong span{color:#f60;font-size:16px}#verificationCode{color:#f60;font-size:24px}#content_bottom{margin-bottom:30px}#content_bottom small{display:block;margin-bottom:20px;font-size:12px;color:#747474}#bottom{width:700px;margin:0 auto}#bottom div{padding:10px 10px 0;border-top:1px solid#ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px}#content_top strong span{font-size:18px;color:#FE4F70}#sign{text-align:right;font-size:18px;color:#FE4F70;font-weight:bold}#verificationCode{height:100px;width:680px;text-align:center;margin:30px 0}#verificationCode div{height:100px;width:680px}</style></head><body><table><tbody><tr><td><div id="top"><table><tbody><tr><td></td></tr></tbody></table></div><div id="content"><div id="content_top"><strong>尊敬的用户：您好！</strong><strong>您正在进行<span>注册账号</span>操作，请10分钟内在验证码中输入以下验证码完成操作：</strong><div id="verificationCode">' . $code . '</div></div><div id="content_bottom"><small>注意：此操作可能会修改您的密码、登录邮箱或绑定手机。如非本人操作，请及时登录并修改密码以保证帐户安全<br>（工作人员不会向你索取此验证码，请勿泄漏！)</small></div></div><div id="bottom"><div><p>此为系统邮件，请勿回复<br>请保管好您的邮箱，避免账号被他人盗用</p><p id="sign">——' . $name . '</p></div></div></td></tr></tbody></table></body>';
    }
}