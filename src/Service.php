<?php

namespace Henan\ThinkSdk;

use think\facade\Route;

class Service extends \Think\Service
{
    public function boot(): void
    {
        $this->registerRoutes(function () {
            $routePrefix = 'sdk'; // 路由前缀
            $routes = function () {
                $controllerNamespace = 'Henan\\ThinkSdk\\controller\\Api'; // 控制器命名空间
                Route::get('api/welcome', $controllerNamespace . '@welcome'); // 欢迎页
            };
            Route::group($routePrefix, $routes)->allowCrossDomain(); // 路由分组 允许跨域
        });
    }
}