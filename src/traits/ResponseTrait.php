<?php

namespace Henan\ThinkSdk\traits;


/**
 * 响应输出特征
 * @author henan
 */
trait ResponseTrait
{
    /**
     * 业务成功(操作成功)
     * @var int
     */
    protected static int $SUCCESS_CODE = 1;

    /**
     * 业务失败(操作失败)
     * @var int
     */
    protected static int $ERROR_CODE = 0;

    /**
     * 无效凭证(缺少凭证或凭证无效)
     * @var int
     */
    protected static int $INVALID_TOKEN_CODE = 402;

    /**
     * 无效账号(账号不存在)
     * @var int
     */
    protected static int $INVALID_ACCOUNT_CODE = 403;

    /**
     * 禁用账号(账号已被禁用)
     * @var int
     */
    protected static int $DISABLE_ACCOUNT_CODE = 405;

    /**
     * 成功响应输出
     * @param mixed $data 业务数据
     * @param string $msg 成功消息
     * @return mixed
     */
    protected function success(mixed $data = null, string $msg = ''): mixed
    {
        throw new \think\exception\HttpResponseException(json(['code' => self::$SUCCESS_CODE, 'msg' => $msg, 'data' => $data]));
    }

    /**
     * 错误响应输出
     * @param string $msg 错误消息
     * @param int $code 错误代码
     * @return mixed
     */
    protected function error(string $msg = '', int $code = 0): mixed
    {
        throw new \think\exception\HttpResponseException(json(['code' => $code, 'msg' => $msg, 'data' => null]));
    }
}