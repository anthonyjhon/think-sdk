<?php

namespace Henan\ThinkSdk\traits;


use think\facade\Validate;
use think\file\UploadedFile;

/**
 * 参数验证特征
 * @author henan
 */
trait ValidateTrait
{
    use ResponseTrait;

    /**
     * 请求参数验证
     * @param array $rules 验证规则
     * @param string $method 请求方法
     * @param bool $auto_null 是否空值自动转NULL
     * @param bool $is_filter 是否过滤非规则参数
     * @return mixed
     */
    protected function check(array $rules, string $method = 'get', bool $auto_null = true, bool $is_filter = false): mixed
    {
        // 请求方法验证
        $this->request->method() != strtoupper($method) && $this->error('请求方法错误');
        // 请求参数获取
        $param = $this->request->$method();
        // 判断是否过滤非规则参数
        if ($is_filter) {
            $keys = array_keys($rules);
            foreach ($param as $key => $value) if (!in_array($key, $keys)) unset($param[$key]);
        }
        // 判断是否自动将空字符串转为null
        if ($auto_null) {
            foreach ($param as $key => $value) if ($value === '') $param[$key] = null;
        }
        // 请求参数验证
        $rule = [];
        foreach ($rules as $key => $value) {
            if (is_int($key)) { // 没有设置默认值
                $arr = explode('|', $value); // 分隔字符
                $field = $arr[0]; // 获取参数名称
                $arr[0] = 'require'; // 替换第一个元素
                $rule[$field] = implode('|', $arr); // 获取验证规则
                $param[$field] = $this->request->param($field);
            } else { // 有设置默认值
                $arr = explode('|', $key); // 分隔字符
                $field = $arr[0]; // 获取参数名称
                $arr[0] = ''; // 替换第一个元素
                !empty(array_filter($arr)) && $rule[$field] = implode('|', array_filter($arr)); // 获取验证规则
                $param[$field] = $this->request->param($field, $value);
            }
        }
        $validate = Validate::rule($rule)->message([]);
        !$validate->check($param) && $this->error($validate->getError());
        return $param;
    }

    /**
     * 检查文件上传
     * @param string $file_name
     * @param array $rules
     * @return array|UploadedFile|null
     */
    protected function checkFile(string $file_name = 'file', array $rules = []): array|UploadedFile|null
    {
        $file = $this->request->file($file_name);
        if (empty($file)) $this->error('没有上传文件');
        $validate = Validate::rule([$file_name => $rules]);
        if (!$validate->check([$file_name => $file])) $this->error($validate->getError());
        return $file;
    }
}