<?php

namespace Henan\ThinkSdk\traits;

use ReflectionClass;

/**
 * 模型特征
 * @author henan
 */
trait ModelTrait
{
    /**
     * 获取常量字典（静态方法）
     * @return array
     */
    public static function getStaticConstDict(): array
    {
        $dict = [];
        $reflectionClass = new ReflectionClass(self::class);
        $constants = $reflectionClass->getConstants();
        foreach ($constants as $name => $item) {
            foreach ($item as $key => $value) $dict[$name][] = ['label' => $value, 'value' => $key];
        }
        return $dict;
    }

    /**
     * 获取常量字典
     * @return array
     */
    public function getConstDict(): array
    {
        $dict = [];
        $reflectionClass = new ReflectionClass($this);
        $constants = $reflectionClass->getConstants();
        foreach ($constants as $name => $item) {
            foreach ($item as $key => $value) $dict[$name][] = ['label' => $value, 'value' => $key];
        }
        return $dict;
    }
}