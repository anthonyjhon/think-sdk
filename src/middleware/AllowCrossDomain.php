<?php

namespace Henan\ThinkSdk\middleware;


use Closure;
use think\Response;

/**
 * 允许跨域请求中间件
 * @author henan
 */
class AllowCrossDomain
{
    public function handle($request, Closure $next)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: *');
        header('Content-type: application/json; charset=UTF-8');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE, HEAD');
        if ($request->isOptions()) return Response::create();
        return $next($request);
    }
}