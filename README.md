<div align="center">
    <img width="128"  src="https://aick-top.oss-cn-shenzhen.aliyuncs.com/images/logo/L01.png" alt="">
</div>

<h1 align="center" style="margin-top: 0;padding-top: 0;">
  Think-SDK
</h1>

<div align="center">
 ThinkPHP8框架开发集成包
</div>

## 🤷‍♀️ 介绍

ThinkPHP8框架开发集成包

## 📌 架构

基于PHP`8.0.0`以上版本和ThinkPHP`8.0`版本框架开发

## 🚀 使用

### 安装

```shell
composer require henan/think-sdk
```

### 配置

[配置文件](./src/config.php)

## 📑 模块

- [Define自定义模块](./docs/Define.md)
- [Helper助手模块](./docs/Helper.md)
- [Middleware中间件模块](./docs/Middleware.md)
- [Model模型模块](./docs/Model.md)
- [Service服务模块](./docs/Service.md)
- [Traits特征模块](./docs/Traits.md)
- [Utils工具模块](./docs/Utils.md)
- [API模块](./docs/API.md)

## 📦 依赖

- topthink/framework
- bingher/ding-bot
- aliyuncs/oss-sdk-php
- phpmailer/phpmailer
- firebase/php-jwt
- phpoffice/phpexcel
- aferrandini/phpqrcode
- topthink/think-queue