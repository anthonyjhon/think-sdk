# API

### 获取欢迎页

#### GET /sdk/api/welcome

#### 返回

```json
{
  "code": 1,
  "msg": "",
  "data": "欢迎使用 Think-SDK"
}
```

